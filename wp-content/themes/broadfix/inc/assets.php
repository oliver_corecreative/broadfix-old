<?php
    function broadfix_assets() {
        wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css',false,'1.1','all');

        //Owl Carousel
        wp_enqueue_style( 'owl.theme.default.min', get_template_directory_uri() . '/css/owl.theme.default.min.css',false,'1.1','all');
        wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/js/owl.carousel.min.js', array ( 'jquery' ), 1.1, true);
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'mobile.navigation', get_template_directory_uri() . '/js/mobile.navigation.js', array ( 'jquery' ), 1.1, true);
        wp_register_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array ( 'jquery' ), 1.1, true);

        //Dequeue scripts
        //wp_dequeue_script('wpsl-gmap.min.js?ver=2.2.7');

        // Localize the script with new data
        $theme_path_array = array(
            'theme_path' => get_template_directory_uri()
        );
        wp_localize_script( 'plugins', 'carousel_nav', $theme_path_array );

        wp_enqueue_script('plugins');
    };

    add_action( 'wp_enqueue_scripts', 'broadfix_assets' );



//Include prettyPhoto as the lightbox site wide

add_action( 'wp_enqueue_scripts', 'frontend_scripts_include_lightbox' );

function frontend_scripts_include_lightbox() {
    global $woocommerce;
    $suffix      = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
    $lightbox_en = get_option( 'woocommerce_enable_lightbox' ) == 'yes' ? true : false;

    if ( $lightbox_en ) {
        wp_enqueue_script( 'prettyPhoto', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
        wp_enqueue_script( 'prettyPhoto-init', $woocommerce->plugin_url() . '/assets/js/prettyPhoto/jquery.prettyPhoto.init' . $suffix . '.js', array( 'jquery' ), $woocommerce->version, true );
        wp_enqueue_style( 'woocommerce_prettyPhoto_css', $woocommerce->plugin_url() . '/assets/css/prettyPhoto.css' );
    }
}

/*
 * Use any number above 10 for priority as the default is 10
 * any number after 10 will load after
 */
add_action( 'wp_enqueue_scripts', 'my_custom_scripts', 100 );
function my_custom_scripts()
{
    wp_deregister_script( 'wp-content/plugins/wp-store-locator/js/wpsl-gmap.min.js' );
    // Now the parent script is completely removed
}
