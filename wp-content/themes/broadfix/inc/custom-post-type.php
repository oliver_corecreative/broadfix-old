<?php
    add_action( 'init', 'register_cpt_testimonial' );

    function register_cpt_testimonial() {

    $labels = array(
    'name' => __( 'Testimonials', 'testimonial' ),
    'singular_name' => __( 'Testimonial', 'testimonial' ),
    'add_new' => __( 'Add New', 'testimonial' ),
    'add_new_item' => __( 'Add New Testimonial', 'testimonial' ),
    'edit_item' => __( 'Edit Testimonial', 'testimonial' ),
    'new_item' => __( 'New Testimonial', 'testimonial' ),
    'view_item' => __( 'View Testimonial', 'testimonial' ),
    'search_items' => __( 'Search Testimonials', 'testimonial' ),
    'not_found' => __( 'No testimonials found', 'testimonial' ),
    'not_found_in_trash' => __( 'No testimonials found in Trash', 'testimonial' ),
    'parent_item_colon' => __( 'Parent Testimonial:', 'testimonial' ),
    'menu_name' => __( 'Testimonials', 'testimonial' ),
    );

    $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-awards',
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'post'
    );

    register_post_type( 'testimonial', $args );
    }