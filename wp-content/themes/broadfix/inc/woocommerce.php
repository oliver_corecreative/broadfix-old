<?php
    function thumbnailcolumns($columns) {
        return 4;
    }

add_filter('woocommerce_product_thumbnails_columns' , 'thumbnailcolumns');
//remove breadcrumbs from before main content
remove_action('woocommerce_before_main_content' , 'woocommerce_breadcrumb' , 20);
//remove excerpt and meta data from single product page
remove_action('woocommerce_single_product_summary' , 'woocommerce_template_single_excerpt' , 20);
remove_action('woocommerce_single_product_summary' , 'woocommerce_template_single_meta' , 40);

//remove product date amnd upsell from single product page
remove_action('woocommerce_after_single_product_summary' , 'woocommerce_output_product_data_tabs' , 10);
remove_action('woocommerce_after_single_product_summary' , 'woocommerce_upsell_display' , 15);

//Add excerpt for product
add_action( 'woocommerce_after_shop_loop_item_title', 'output_product_excerpt', 20 );

function output_product_excerpt()
{
    global $post;
    echo $post->post_excerpt;
};

//Change the read more link on the products
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_archive_custom_cart_button_text' );

function woo_archive_custom_cart_button_text() {

    return __( '', 'woocommerce' );

}