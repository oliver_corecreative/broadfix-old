<?php
/**
 * Template part for displaying page content in news-page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package broadfix
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header yellow-bg">
        <div class="container">
            <h1 class="entry-title"><?php echo get_the_title(get_option('page_for_posts')); ?> AT BROADFIX</h1>
        </div>
        <div class="container">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
        </div>
    </header><!-- .entry-header -->

	<div class="entry-content">
		<div class="container">	
			<div class="news-posts">

                <?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

				<ul class="single-post">
                    <li class="news-title"><h2 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2></li>
                    <ul class="news-details">
                        <li><?php the_date('d-m-Y'); ?></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/category-icon.svg" alt="Category Icon" class="category-icon" /><?php the_category( ', ' ); ?></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/author-icon.svg" alt="Author Icon" class="author-icon" /><?php the_author(); ?></li>
                    </ul>
                    <li class="news-image"><a href="<?php the_permalink() ?>"><?php the_post_thumbnail(); ?></a></li>
                    <li class="news-more"><?php the_excerpt(__('(more…)')); ?></li>
				</ul>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>

			</div>

			<div class="side-bar">
                <?php get_template_part( 'sidebar' ); ?>
			</div>
        </div>
			
	</div>

</article><!-- #post-## -->
