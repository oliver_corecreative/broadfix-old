<div class="mailing-bar-container">
    <div class="mailing-bar">
        <strong>Enjoy this article?</strong> We’ve got lots more to share with you!
        <?php echo do_shortcode("[cm_form form_id='cm_58c8fec7e8f84']"); ?>
    </div>
</div>