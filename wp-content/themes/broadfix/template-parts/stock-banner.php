<!--Banner for standard pages
to add slider in again - change div class carousel to owl-carousel.
-->
<div class="slide-container">
    <div class="carousel">
        <div class="slide">
            <img src="<?php the_field('banner_image'); ?>" />
            <h1 class="stock-broadfix">Join the Broadfix Revolution today!</h1>
            <h3>Great reasons our distributors stock broadfix</h3>
        </div>
    </div>
</div>

<div id="goldenShim" class="golden-shim">
    <?php echo do_shortcode("[contact-form-7 id=4872 html_class='goldenShim']"); ?>
</div>




