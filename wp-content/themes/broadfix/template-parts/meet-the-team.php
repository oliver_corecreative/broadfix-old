<div class="text-container blue-bg">
    <div class="container">
        <h2><span class="white">Our People</span>&nbsp;<span class="desktop-only">&nbsp;&nbsp;&nbsp;</span><span class="yellow">Introducing a team dedicated to living the Broadfix Brand...</span></h2>
    </div>
</div>
<div class="meet-the-team-container">
    <div class="container">
        <div class="slide-container">

            <div class="owl-carousel">
                <?php foreach (get_field('team_members') as $item): ?>

                    <div class="slide">
                        <div class="profile-picture">
                            <img src="<?= $item['profile_picture']['url']; ?>" alt="<?= $item['name']; ?>" class="team-image"/>
                        </div>

                        <div class="profile-container">
                            <div class="profile">
                                <h1 class="uppercase profile-intro"><?= $item['intro']; ?></h1>
                                <p><?= $item['profile']; ?></p>
                                <p class="large"><?= $item['name']; ?></p>
                                <ul class="social-icons">
                                    <li><a href="<?php the_field('twitter', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.svg" alt="Twitter icon" class="social-icon" /></a></li>
                                    <li><a href="<?php the_field('facebook', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-icon.svg" alt="Facebook icon" class="social-icon" /></a></li>
                                    <?php if( $item['team_linkedin'] ): ?>
                                        <li><a href="<?= $item['team_linkedin']; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.svg" alt="Linkedin icon" class="social-icon" /></a></li>
                                    <?php else : ?>
                                        <li><a href="<?php the_field('linkedin', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.svg" alt="Linkedin icon" class="social-icon" /></a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>

