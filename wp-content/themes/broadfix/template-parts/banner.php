<!--Banner for standard pages
to add slider in again - change div class carousel to owl-carousel.
-->
<div class="slide-container">
    <div class="carousel">
        <div class="slide">
            <img src="<?php the_field('banner_image'); ?>" />
        </div>
    </div>
</div>