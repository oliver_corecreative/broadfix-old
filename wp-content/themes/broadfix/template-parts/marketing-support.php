<div class="text-container blue-bg">
    <div class="container">
        <h2><span class="white">Become a Broadfix Distributor...</span>&nbsp;<span class="yellow">Call our team now on <?php the_field('phone_number', 'option'); ?> or <a href="#distributor" class="yellow">click here</a></span></h2>
    </div>
</div>
<div class="marketing-container">
    <div class="container">
        <div class="marketing-picture">
            <img src="<?php the_field('marketing_image'); ?>" alt="Broadfix Marketing Material Image" />
        </div>
        <div class="marketing-content">
            <?php the_field('marketing_content'); ?>
        </div>
    </div>
</div>