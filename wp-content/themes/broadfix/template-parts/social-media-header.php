<div class="container">
    <div class="social-container">
        <ul class="social">
            <li><a href="<?php the_field('twitter', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.svg" class="social-icon hover" alt="Broadfix Twitter"/></a></li>
            <li><a href="<?php the_field('facebook', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-icon.svg" class="social-icon hover" alt="Broadfix Facebook"/></a></li>
            <li><a href="<?php the_field('linkedin', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.svg" class="social-icon hover" alt="Broadfix Linkedin"/></a></li>
            <li><a href="<?php the_field('instagram', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-icon.svg" class="social-icon hover" alt="Broadfix Instagram"/></a></li>
            <li><a href="<?php the_field('googleplus', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/googleplus-icon.svg" class="social-icon hover" alt="Broadfix Google Plus"/></a></li>
            <li><a href="<?php the_field('youtube', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/youtube-icon.svg" class="social-icon hover" alt="Broadfix Youtube"/></a></li>
        </ul>
        <?php get_template_part( 'template-parts/search' ); ?>
        <div class="stockist-link"><a href="<?php echo home_url(); ?>/find-a-stockist/"/>Find a stockist <img src="<?php echo get_template_directory_uri(); ?>/img/right-arrow.png" class="stockist-icon hover" alt="Go arrow"/></div>
    </div>
</div>