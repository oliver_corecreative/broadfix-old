<div class="did-you-know-container">
    <div class="container">
        <div class="icon">
            <img src="<?php echo get_template_directory_uri(); ?>/img/did-you-know-icon.png" alt="Did you know icon" />
        </div>
        <div class="slide-container">
            <div class="owl-carousel">
                <?php foreach(get_field('did_you_know') as $item): ?>
                <div class="slide">
                    <h4><?= $item['fact']; ?></h4>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>