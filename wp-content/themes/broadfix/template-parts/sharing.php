<ul class="share-buttons">
    <li>
        <a href="https://twitter.com/intent/tweet?source=http%3A%2F%broadfixproducts.co.uk&text=:%20http%3A%2F%2Fbroadfixproducts.co.uk&via=Broadfix" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/twitter-icon.svg" class="share-icon" alt="Share this article on Twitter" />
        </a>
    </li>
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fbroadfixproducts.co.uk&t=" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&t=' + encodeURIComponent(document.URL)); return false;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/facebook-icon.svg" class="share-icon" alt="Share this article on Facebook" />
        </a>
    </li>
    <li>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Fbroadfixproducts.co.uk&title=&summary=&source=http%3A%2F%2Fbroadfixproducts.co.uk" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(document.URL) + '&title=' +  encodeURIComponent(document.title)); return false;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.svg" class="share-icon" alt="Share this article on Linkedin" />
        </a>
    </li>
    <li>
        <a href="https://plus.google.com/share?url=http%3A%2F%2Fbroadfixproducts.co.uk" target="_blank" title="Share on Google+" onclick="window.open('https://plus.google.com/share?url=' + encodeURIComponent(document.URL)); return false;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/googleplus-icon.svg" class="share-icon" alt="Share this article on Google plus" />
        </a>
    </li>
    <li>
        Share this article
    </li>
</ul>