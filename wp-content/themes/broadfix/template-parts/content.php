<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package broadfix
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header yellow-bg">
        <div class="container">
            <h1 class="entry-title"><?php echo get_the_title(get_option('page_for_posts')); ?> AT BROADFIX</h1>
        </div>
        <div class="container">
            <div class="breadcrumbs">
                <?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
            </div>
        </div>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <div class="container">
            <div class="news-posts">
                <ul>
                    <li class="news-title"><h2 class="title"><?php the_title(); ?></h2></li>
                    <ul class="news-details">
                        <li><?php the_date('d-m-Y'); ?></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/category-icon.svg" class="category-icon" /><?php the_category( ', ' ); ?></li>
                        <li><img src="<?php echo get_template_directory_uri(); ?>/img/author-icon.svg" class="author-icon" /><?php the_author(); ?></li>
                    </ul>
                    <li class="news-image"><?php the_post_thumbnail(); ?></li>
                    <li class="news-more"><?php the_content(); ?></li>
                </ul>
                <footer class="entry-footer">
                    <?php get_template_part("template-parts/sharing"); ?>
                    <?php get_template_part("template-parts/email"); ?>
                    <a href="<?= get_post_type_archive_link("post"); ?>" class="back-button">Back to articles</a>
                </footer><!-- .entry-footer -->
            </div>
            <div class="side-bar">
                <?php get_template_part( 'sidebar' ); ?>
            </div>
        </div>
    </div><!-- .entry-content -->


</article><!-- #post-## -->