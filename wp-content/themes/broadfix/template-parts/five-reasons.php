<div class="five-reasons-container">
    <div class="container">
        <ul>
        <?php foreach (get_field('five_reasons') as $item): ?>
            <li><?= $item['reason']; ?></li>
        <?php endforeach; ?>
        </ul>
        <div class="broadfix-boy">
            <img src="<?php echo get_template_directory_uri(); ?>/img/broadfix-boy-reasons.png" alt="Broadfix Boy Image" />
        </div>
    </div>
</div>