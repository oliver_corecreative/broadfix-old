<div class="cta-block-container">
    <div class="cta-block">
        <ul>
            <a href="<?php echo home_url(); ?>/product-category/broadfix-shims/" alt="View our product range">
                <li>
                    <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/bag.png" alt="Broadfix Shims Bag" class="img-one" ></div>
                    <div class="cta-block-text">Explore the broadfix range now
                        <p class="blue button med hover">Tell me more...</p>
                    </div>
                </li>
            </a>
            <a href="<?php echo home_url(); ?>/about-our-products" alt="Discover our products">
                <li>
                    <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/broadfix-boy.png" alt="Broadfix Boy" class="img-two"></div>
                    <div class="cta-block-text">5 Great reasons to shim it with broadfix...
                        <p class="blue button med hover">Tell me more...</p>
                    </div>
                </li>
            </a>
            <a href="<?php echo home_url(); ?>/stock-broadfix-today" alt="Stock Broadfix products">
                <li class="last">
                    <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/stand.png" alt="Broadfix Stand" class="img-three"></div>
                    <div class="cta-block-text">Join the revolution! Become a distributor
                        <p class="blue button med hover">Tell me more...</p>
                    </div>
                </li>
            </a>
        </ul>
    </div>
</div>