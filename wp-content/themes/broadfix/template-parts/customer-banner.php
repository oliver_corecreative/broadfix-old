<!--Banner for customer section on about products page
-->
<div class="customer-container">
    <div class="container">
        <h1>WE HAVE A PRODUCT FOR EVERYONE, FIND YOUR TRADE BELOW</h1>
        <div class="slide-container">
            <div class="customer-carousel">
                <?php foreach(get_field('customer_types') as $item): ?>
                <div class="slide">
                    <div class="customer-column">
                        <div class="customer-title">
                            <h3 class="uppercase"><?= $item['customer_name']; ?></h3>
                            <p><?= $item['customer_description']; ?></p>
                        </div>
                        <div class="customer-image">
                            <img src="<?= $item['image']['url']; ?>" alt="<?= $item['customer_name']; ?>" />
                            <a href="<?php echo home_url(); ?><?= $item['link']; ?>" class="button yellow-bg blue view-range">
                                <?= $item['button_text']; ?>
                            </a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>