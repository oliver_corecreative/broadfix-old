<?php
/**
 * Template part for displaying page content in news-page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package broadfix
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header yellow-bg">
		<div class="container">
            <h1 class="entry-title"><?php echo get_the_title(get_option('page_for_posts')); ?> AT BROADFIX</h1>
		</div>
		<div class="container">
            	<div class="breadcrumbs">
					<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
				</div>
			</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="container">	
			<div class="news-posts">
                <?php
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        //
                        // Post Content here
                        //
                    } // end while
                } // end if
                ?>
			</div>

			<div class="side-bar">
                <?php get_template_part( 'sidebar' ); ?>
			</div>
        </div>
			
	</div>

</article><!-- #post-## -->
