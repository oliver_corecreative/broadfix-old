<?php
$args=array(
    'post_type'=>'testimonial',
    'orderby'=>'rand',
    'posts_per_page'=>'1',
); $testimonial=new WP_Query($args); while ($testimonial->have_posts()) : $testimonial->the_post();
    ?>

<div class="text-container blue-bg">
    <div class="container">
        <?php if( get_field('user_heading') ): ?>
            <h2><span class="yellow"><?php the_field('user_heading'); ?></h2>
        <?php else : ?>
        <ul class="testimonial-bar">
            <li>Builders</li>
            <li>Plumbers</li>
            <li>Carpenters</li>
            <li>Electricians</li>
            <li class="last white">...Simply Shim it with Broadfix</li>
        </ul>
        <?php endif; ?>
    </div>
</div>

<div class="testimonial-block-container">
    <div class="testimonial block">
        <div class="testimonial-button">
            <a href="<?php echo home_url(); ?>/about-our-products" class="button yellow-bg blue low">Read about the benefits</a>
        </div>
        <div class="testimonial-text">
            <div>
                <img src="<?php the_field('product_image'); ?>" />
            </div>
            <p>
                <?php the_excerpt();?>
            </p>
            <p class="attribute">
                <?php the_title(); ?>
            </p>
        </div>
    </div>
</div>

<?php endwhile; wp_reset_postdata(); ?>



