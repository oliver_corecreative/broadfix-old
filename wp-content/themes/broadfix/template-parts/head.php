<!--Header-->
<header class="banner">
    <nav class="top">
        <?php get_template_part( 'template-parts/social-media-header' ); ?>
    </nav>

    <div class="branding-container">
        <div class="branding">
            <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/broadfix-logo.png" alt="Go to the Broadfix Homepage" /></a>
        </div>
        <div class="mobile-menu">
            <button class="hamburger blue" type="button">
            </button>
        </div>
    </div>

    <div class="nav-wrapper">
        <nav id="site-navigation" class="main-navigation" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
        </nav><!-- #primary-navigation -->

        <nav id="site-navigation" class="secondary-navigation" role="navigation">
            <?php wp_nav_menu( array( 'menu' => 'secondary-menu' ) ); ?>
        </nav><!-- #secondary-navigation -->
    </div>



    <?php

    if ( is_front_page() ) : ?>
        <div class="slide-container">
            <div class="owl-carousel">
                <?php foreach(get_field('homepage_banner') as $item): ?>
                    <div class="slide">
                        <a href="<?= $item['page_link']; ?>"><img src="<?= $item['banner']['url']; ?>" alt="<?= $item['alt']; ?>" /></a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="mailing-bar-container yellow-bg">
            <div class="mailing-bar">
                <?php echo do_shortcode("[cm_form form_id='cm_58c0247c40221']"); ?>
            </div>
        </div>

        <?php get_template_part('template-parts/explore'); ?>

    <?php else : ?>
    <?php endif; ?>

</header>
<!--/Header-->