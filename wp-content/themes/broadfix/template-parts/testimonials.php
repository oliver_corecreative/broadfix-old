<div class="testimonials-container">
    <div class="testimonials block">
        <h5 class="uppercase">What our customers are saying</h5>
        <div class="slide-container">
            <div class="testimonial-carousel">
                <?php foreach(get_posts([ 'post_type' => 'testimonial' ]) as $testimonial): ?>
                <div class="slide">
                    <?= $testimonial->post_content; ?>
                    <p class="attribute"><?= $testimonial->post_title; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>