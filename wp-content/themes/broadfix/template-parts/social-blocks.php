<div class="social-block-container">
    <div class="container">
        <h1 class="uppercase">Yes it's true, we have a Tuk-Tuk, grass tables and a slide</h1>
        <?php foreach(get_field('social_blocks') as $item): ?>
            <div class="column">
                <div class="image">
                    <img src="<?= $item['image']['url']; ?>" alt="<?= $item['alt']; ?>" />
                </div>
                <?= $item['text']; ?>
            </div>
        <?php endforeach; ?>

        <h5>There's loads more happening right now on our social pages, start following today!</h5>
        <?php get_template_part( 'template-parts/social-media'); ?>
    </div>
</div>