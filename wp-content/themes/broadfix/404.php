<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">

				<header class="entry-header yellow-bg">
					<div class="container">
						<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'broadfix' ); ?></h1>
					</div>
					<div class="container">
						<div class="breadcrumbs">
							<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
						</div>
					</div>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<div class="container">
						<div class="block">
							<p>To help you find what you're looking for, <a href="<?php echo home_url(); ?>">return to the homepage</a> or use the search box below.</p>
						</div>
						<div class="block">
							<?php get_product_search_form(); ?>
						</div>
						<div class="block block-search">
							<a href="/shop/" class="site-button blue-bg white hover">View our full product range</a>
						</div>
					</div>
				</div>

				<div class="stockist" id="stockists">
					<?php get_template_part('template-parts/stockist'); ?>
					<h5>For help call us now on <?php the_field('phone_number', 'option'); ?>, 8:30am to 4:30pm Monday to Friday</h5>
					<?php get_template_part('template-parts/explore'); ?>
				</div>

			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
