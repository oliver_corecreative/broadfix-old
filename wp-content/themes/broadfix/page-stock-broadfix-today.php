<?php
/**
 * The template for Stock Broadfix.
 *
 * @package broadfix
 */

get_header(); ?>

    <div id="primary" class="content-area stock-broadfix">
        <main id="main" class="site-main" role="main">
            <div class="breadcrumb-container">
                <div class="container">
                    <?php get_template_part('template-parts/breadcrumbs'); ?>
                </div>
            </div>

            <?php get_template_part( 'template-parts/stock-banner' ); ?>

            <div class="container">
                <div class="join-us-container">
                    <div class="join-us">
                        <img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/join-us.png" alt="Join Broadfix" />
                    </div>
                </div>
                <div class="reasons">
                    <?php get_template_part( 'template-parts/content-page', 'page' ); ?>
                </div>
            </div>

            <?php get_template_part( 'template-parts/marketing-support' ); ?>

            <div class="container">
                <div class="distributor-info">
                    <?php the_field('distributor_intro'); ?>
                </div>
                <div class="distributor-form">
                    <p class="contact-intro" id="distributor">Join the Broadfix Revolution and become a Distributor today</p>
                    <?php echo do_shortcode("[contact-form-7 id=199]"); ?>
                </div>
            </div>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
