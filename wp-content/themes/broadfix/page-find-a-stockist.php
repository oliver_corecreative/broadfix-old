<?php
/**
 * The template for Find a Stockist page.
 *
 * @package broadfix
 */

get_header(); ?>

    <div id="primary" class="content-area find-a-stockist">
        <main id="main" class="site-main" role="main">
            <?php
                global $post;
                $queried_object = get_queried_object();

                echo do_shortcode( '[wpsl]' );
            ?>

            <?php get_template_part('template-parts/explore'); ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
