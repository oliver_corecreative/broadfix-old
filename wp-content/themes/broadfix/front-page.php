<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="site-main" role="main">

            <?php get_template_part('template-parts/testimonial-block')?>

            <div class="news-container">
                <div class="news block">
                    <div class="news-column no-margin-left">
                        <?php $the_query = new WP_Query( 'posts_per_page=1' ); ?>
                        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                        <a href="<?php the_permalink() ?>">
                            <div>
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="bottom">
                                <div class="post-title"><?php the_title(); ?> </div>
                            </div>
                        </a>
                        <?php
                            endwhile;
                            wp_reset_postdata();
                        ?>
                    </div>

                    <div class="news-column no-margin-right">
                        <a href="<?php echo home_url(); ?>/stock-broadfix-today">
                            <div>
                                <div class="post-title no-margin-top">JOIN THE REVOLUTION! <br />BECOME A STOCKIST TODAY</div>
                            </div>
                            <div class="bottom">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/stock-broadfix.jpg" alt="Stock Broadfix Image" />
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <?php get_template_part('template-parts/stockist'); ?>

        </div>

            <div class="latest-news-container">
                <div class="latest-news block">
                    <?php $posts = get_posts('orderby=rand&numberposts=1'); foreach($posts as $post) {?> 
                        <h5 class="uppercase"><?php the_title();?></h5>
                        <?php echo strip_shortcodes(wp_trim_words( get_post_field('post_content', $post->ID), 80 )); } ?> 
                    <a href="whats-happening" class="news-button blue-bg white hover">More latest news</a>
                </div>
            </div>

        </div>
    </div><!-- #primary -->

<?php
get_footer();