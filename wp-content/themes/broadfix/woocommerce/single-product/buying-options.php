<div class="buying-options">
    <ul>
        <?php if( get_field('available_sizes') ): ?>
        <li>
            <span class="option-title">Available Sizes&nbsp;</span><?php the_field('available_sizes'); ?>
        </li>
        <?php endif ?>
        <?php if( get_field('buying_options') ): ?>
        <li class="last">
            <span class="option-title">Buying options&nbsp;</span><?php the_field('buying_options'); ?>
        </li>
        <?php endif ?>
    </ul>
</div>