<div class="cta-block-container">
    <div  class="cta-block">
        <ul>
            <li>
                <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/share-your-ideas.png" alt="Hand holding mobile phone" class="img-four" ></div>
                <div class="cta-block-text">share your broadfix ideas and win a gift!
                    <a href="<?php echo home_url(); ?>/get-in-touch" alt="#" class="blue button med">Find out more</a>
                </div>
            </li>
            <li>
                <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/broadfix-boy.png" alt="Broadfix Boy" class="img-two"></div>
                <div class="cta-block-text">watch out for our latest promotions
                    <a href="<?php echo home_url(); ?>/whats-happening" alt="#" class="blue button med">Take a look...</a>
                </div>
            </li>
            <li class="last">
                <div class="cta-block-image"><img src="<?php echo home_url(); ?>/wp-content/uploads/2016/10/stand.png" alt="Broadfix Stand" class="img-three"></div>
                <div class="cta-block-text">Join the revolution! Become a distributor
                    <a href="<?php echo home_url(); ?>/stock-broadfix-today" alt="#" class="blue button med">Tell me more...</a>
                </div>
            </li>
        </ul>
    </div>
</div>