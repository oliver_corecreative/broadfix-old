        <div class="breadcrumbs">
            <?php
            $args = array(
                'delimiter' => ' > ',
            );
            ?>
            <?php woocommerce_breadcrumb( $args ); ?>
        </div>
        <div class="back">
            <?php
            $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
            echo "<a href='$url'>&#60; Back</a>";
            ?>
        </div>


