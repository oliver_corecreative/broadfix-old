<div class="gallery-bar yellow-bg">
    <span class="gallery-title">More product images&nbsp;<br class="mobileonly" /></span>Got your own images? Submit them now and win a prize&nbsp;
    <div id="submissionForm" class="submit-images">
        <?php echo do_shortcode("[contact-form-7 id=338 html_class='submissionForm']"); ?>
    </div>
    <a data-rel="prettyPhoto" href="#submissionForm"><i class="icon-arrow-right"></i></a>
</div>

<div class="gallery">
    <?php do_action( 'woocommerce_product_thumbnails' ); ?>
</div>
