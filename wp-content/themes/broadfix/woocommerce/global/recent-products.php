<?php
/**
  *Recent Products for when no products to show
*/

array(
    'per_page' => '8',
    'columns' => '4',
    'orderby' => 'date',
    'order' => 'desc'
)
?>

<h1>OTHER POPULAR BROADFIX PRODUCTS</h1>
<?php echo do_shortcode("[recent_products per_page=\"8\" columns=\"4\"]"); ?>