<form role="search" method="get" id="searchform" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
    <div>
        <label class="screen-reader-text" for="s"><?php _e( 'Search for:', 'woocommerce' ); ?></label>
        <input type="search" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php _e( 'I&#39;M LOOKING FOR', 'woocommerce' ); ?>" class="search-field" />
        <input type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Search' ); ?>" />
        <input type="hidden" name="post_type" value="product" />
    </div>
</form>