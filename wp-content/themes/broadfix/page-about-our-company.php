<?php
/**
 * The template for About our company.
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area about-our-company">
		<main id="main" class="site-main" role="main">
            <div class="breadcrumb-container">
                <div class="container">
                    <?php get_template_part('template-parts/breadcrumbs'); ?>
                </div>
            </div>

            <?php get_template_part( 'template-parts/banner' ); ?>

            <div class="container">
                <?php get_template_part( 'template-parts/content-page', 'page' ); ?>
            </div>

            <?php get_template_part( 'template-parts/meet-the-team' ); ?>

            <?php get_template_part( 'template-parts/social-blocks' ); ?>

        </main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
