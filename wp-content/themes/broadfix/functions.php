<?php
/**
 * broadfix functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package broadfix
 */

//Manually change the sites URL
//Change back to http://localhost:8888/broadfix once finished
//Use the mac url to be able to view on external devices on same network
//update_option( 'siteurl', 'http://192.168.1.47:8888/broadfix/' );
//update_option( 'home', 'http://192.168.1.47:8888/broadfix/' );

if ( ! function_exists( 'broadfix_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function broadfix_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on broadfix, use a find and replace
	 * to change 'broadfix' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'broadfix', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'broadfix' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'broadfix_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'broadfix_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function broadfix_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'broadfix_content_width', 640 );
}
add_action( 'after_setup_theme', 'broadfix_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function broadfix_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'broadfix' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'broadfix' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'broadfix_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function broadfix_scripts() {
	wp_enqueue_style( 'broadfix-style', get_stylesheet_uri() );

	wp_enqueue_script( 'broadfix-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'broadfix-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'broadfix_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 *Load assets PHP file
 */
require get_template_directory() . '/inc/assets.php';

/**
 *Load custom post type PHP file
 */
require get_template_directory() . '/inc/custom-post-type.php';

/**
 *Load custom woocommerce functions
 */
require get_template_directory() . '/inc/woocommerce.php';

/**
 *Load custom Store location template
 */
require get_template_directory() . '/inc/store-locator.php';



/**
 *Modify Read more link
 */
function modify_read_more_link() {
    global $post;
    return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read More</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
	global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Limit Excerpt to 30 words
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Custom Options
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}


//Number of products per page on WooCommerce
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 24;' ), 20 );

//Add slug to body class
//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

//
//Store Locator custom options
//
//Add extra custom field for stocked products on stockist page
add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {

	$meta_fields[__( 'Stocked Products', 'wpsl' )] = array(
		'products' => array(
			'label' => __( 'Products', 'wpsl' ),
			'type' => 'wp_editor'
		)
	);

	return $meta_fields;
}

//Include the Custom Data In the JSON Response
add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

	$store_fields['wpsl_products'] = array(
		'name' => 'products',
		'type' => 'textarea'
	);

	return $store_fields;
}

//Custom store listing template
add_filter( 'wpsl_listing_template', 'custom_listing_template' );

function custom_listing_template() {

	global $wpsl, $wpsl_settings;

	$listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
	$listing_template .= "\t\t" . '<div class="store-listing">' . "\r\n";

	$listing_template .= "\t\t" . '<div class="store">' . "\r\n";
	// Check if we need to show the distance.
	if ( !$wpsl_settings['hide_distance'] ) {
		$listing_template .= "\t\t" . '<span class="wpsl-distance"><%= distance %> ' . esc_html( $wpsl_settings['distance_unit'] ) . '</span>' . "\r\n";
	}

	$listing_template .= "\t\t\t" . '<p><%= thumb %>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<span class="listing">' . wpsl_store_header_template( 'listing' ) . '</span>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address %></span>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<span class="wpsl-street"><%= address2 %></span>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<% } %>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<span>' . wpsl_address_format_placeholders() . '</span>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<span class="wpsl-country"><%= country %></span>' . "\r\n";
	$listing_template .= "\t\t\t" . '</p>' . "\r\n";
	$listing_template .= "\t\t\t" . '<%= createDirectionUrl() %>' . "\r\n";

	$listing_template .= "\t\t" . '</div>' . "\r\n";

	$listing_template .= "\t\t" . '<div class="products">' . "\r\n";

	// Show the stocked products data if it exist.
	$listing_template .= "\t\t\t" . '<% if ( products ) { %>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<p class="title">Stocked Products</p>' . "\r\n";
	$listing_template .= "\t\t\t\t" . '<%= products %>' . "\r\n";
	$listing_template .= "\t\t\t" . '<% } %>' . "\r\n";

	$listing_template .= "\t\t" . '</div>' . "\r\n";

	$listing_template .= "\t" . '</li>' . "\r\n";


	return $listing_template;
}