<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package broadfix
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer yellow-bg" role="contentinfo">
		<div class="container yellow-bg">
            <div class="branding footer-logo">
                <a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/broadfix-logo-footer.png" alt="Broadfix Logo" /></a>
            </div>
            <div class="footer-column address">
                <span class="address-title">Broadfix UK Head Office</span><br />
                <?php the_field('address', 'option'); ?><br />
                Telephone <?php the_field('phone_number', 'option'); ?><br />
                Fax <?php the_field('fax_number', 'option'); ?><br />
                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
            </div>
            <div class="footer-column">
                <span class="address-title">OUR PRODUCT RANGES</span><br />
                <?php wp_nav_menu( array( 'menu' => 'secondary-menu' ) ); ?>
            </div>
            <div class="footer-column">
                <span class="address-title">Explore our website</span><br />
                <?php wp_nav_menu( array( 'menu' => 'primary-menu' ) ); ?>
            </div>
            <div class="footer-column">
                <span class="address-title">The latest news</span><br />
                <div class="feed">
                    <?php echo do_shortcode("[rotatingtweets screen_name='Broadfix' links_in_new_window='1' rotation_type='scrollHorz' show_meta_screen_name='0' show_meta_via='0' show_meta_timestamp='0']"); ?>
                </div>
                <a href="<?php the_field('twitter', 'option'); ?>"><strong>Join the conversation <img src="<?php echo get_template_directory_uri(); ?>/img/right-arrow.png" class="twitter-link" alt="Go arrow"/></strong></a>
            </div>
            <div class="details yellow-bg">
                <ul>
                    <li>Content Copyright &copy; 2016 Broadfix</li>
                    <li><a href="<?php echo home_url(); ?>/privacy-policy">Privacy Policy</a></li>
                    <li><a href="<?php echo home_url(); ?>/terms-conditions">Terms &amp; Conditions</a></li>
                    <li><?php the_field('company_reg_number', 'option'); ?></li>
                    <li><?php the_field('vat_number', 'option'); ?></li>
                    <li class="last"><a href="https://corecreative.co.uk/">Site by Core Creative Limited</a></li>
                </ul>
            </div>
        </div> <!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
