<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

            <header class="entry-header yellow-bg">
                <div class="container">
                    <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
                </div>
                <div class="container">
                    <?php get_template_part('template-parts/breadcrumbs'); ?>
                </div>
            </header><!-- .entry-header -->

			<div class="container">
                <?php get_template_part( 'template-parts/content', 'page' ); ?>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
