//Owl Carousel
(function($) {
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                navText: ["<img src='"+carousel_nav.theme_path+"/img/left-arrow.svg' class='prev'>","<img src='"+carousel_nav.theme_path+"/img/right-arrow.svg' class='next'>"]
            }
        }
    })
})( jQuery );


//About Product Page Carousel
(function($) {
    $('.customer-carousel').owlCarousel({
        loop:true,
        margin:20,
        responsiveClass:true,
        singleItem: true,
        responsive:{
            0:{
                items:1,
                nav:false,
                slideBy: 1
            },
            600:{
                items:2,
                nav:false,
                slideBy: 2,
            },
            800:{
                items:2,
                nav:false,
                slideBy: 2,
            },
            1020:{
                items:2,
                nav:true,
                slideBy: 2,
                navText: ["<img src='"+carousel_nav.theme_path+"/img/left-arrow.svg' class='prev'>","<img src='"+carousel_nav.theme_path+"/img/right-arrow.svg' class='next'>"]
            }
        }
    })
})( jQuery );


//Testimonials Carousel
(function($) {
    $('.testimonial-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        singleItem: true,
        autoplay: true,
        dots:true,
        autoHeight:false,
        responsive:{
            0:{
                items:1,
                slideBy: 1
            }
        }
    })
})( jQuery );

//    alert($('.owl-carousel').find('.slide').length > 1);

//Open store locator on product page
$('#btn').on('click', function(event){
    $('.stockist').toggleClass('open');
});

//Store Locator open on click
$( "#wpsl-search-btn" ).click(function() {
    $( '#wpsl-result-list' ).toggleClass( "reveal-height" );
});
