<?php
/**
 * The template for About our Product.
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area about-our-products">
		<main id="main" class="site-main" role="main">
            <div class="breadcrumb-container yellow-bg">
                <div class="container">
                    <?php get_template_part('template-parts/breadcrumbs'); ?>
                </div>
            </div>

            <?php get_template_part( 'template-parts/banner' ); ?>

            <div class="container">
                <?php get_template_part( 'template-parts/content-page', 'page' ); ?>
            </div>

            <?php get_template_part( 'template-parts/five-reasons' ); ?>

            <?php get_template_part( 'template-parts/did-you-know' ); ?>

            <?php get_template_part( 'template-parts/customer-banner' ); ?>

            <?php get_template_part( 'template-parts/testimonials' ); ?>

        </main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
