<?php
/**
 * The template for Contact Page.
 *
 * @package broadfix
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main contact" role="main">
            <div class="breadcrumb-container">
                <div class="container">
                    <?php get_template_part('template-parts/breadcrumbs'); ?>
                </div>
            </div>

            <div class="container">
                <?php get_template_part( 'template-parts/content-page', 'page' ); ?>
                <p class="contact-intro">It'll be great to hear from you...</p>
                <?php echo do_shortcode("[contact-form-7 id=181]"); ?>
            </div>
        </main>
    </div>
    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <div class="text-container blue-bg">
                <div class="container">
                    <h2><span class="white">Where it all happens...</span> <span class="yellow">Broadfix, Apex House, Coldnose Road, Hereford HR2 6JL</span></h2>
                </div>
            </div>

            <?php echo do_shortcode("[put_wpgm id=1]"); ?>

            <div class="stockist" id="stockists">
                <?php get_template_part('template-parts/stockist'); ?>
                <h5>For help call us now on <?php the_field('phone_number', 'option'); ?>, 8:30am to 4:30pm Monday to Friday</h5>
                <?php get_template_part('template-parts/explore'); ?>
            </div>

        </main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
